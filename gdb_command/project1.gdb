define gdb_init
	set print pretty on	

	target remote localhost:1234
	b thread_init
	b thread_create
	b thread_awake
	b thread_sleep
	
	continue
	display_init
end

define display_init
	display list_size(&all_list)
	display list_size(&ready_list)
	display running_thread()->name
end

define cc
	continue
	name_list
end


define tid_ready_list
	
	set $size = list_size(&ready_list)

	set $offset = &((struct thread *)0)->elem
	
	set $node = ready_list.head
	
	set $N = 0
	while $size-- != 0
		set $node=$node.next
		printf "%dth : %d\n" , ++$N , ((struct thread *)((int)$node-(int)$offset))->tid
	end
end

define name_list
	name_all_list
	print "= = = = ="
	name_ready_list
	print "= = = = ="
	name_sleep_list
end

define name_ready_list
print "<ready process>"
	set $size = list_size(&ready_list)
	set $offset = &((struct thread *)0)->elem
	set $node = ready_list.head
	
	set $N = 0
	while $size-- != 0
		set $node=$node.next
		print ((struct thread *)((int)$node-(int)$offset))->name
	end
end

define name_all_list
print "<all process>"
	set $size = list_size(&all_list)
	set $offset = &((struct thread *)0)->allelem
	set $node = all_list.head
	
	while $size-- != 0
		set $node=$node.next
		print ((struct thread *)((int)$node-(int)$offset))->name
	end
end

define name_sleep_list
print "<sleeping process>"	
	set $size = list_size(&sleep_list)
	set $offset = &((struct thread *)0)->sleepelem
	set $node = sleep_list.head
	
	set $N = 0
	while $size-- != 0
		set $node=$node.next
		print ((struct thread *)((int)$node-(int)$offset))->name
	end
end
